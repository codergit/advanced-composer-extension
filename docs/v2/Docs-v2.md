# Advanced Composer 2.* Documentation

<br>

## Table of Contents
- [Features](#features)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Quick-start Guide](#quick-start-guide)
  * [Enable or disable extension features](#enable-or-disable-extension-features)
  * [Change the default behavior of Thunderbird editor](#change-the-default-behavior-of-thunderbird-editor)
  * [Customize the quote header](#customize-the-quote-header)
  * [Create a message template](#create-a-message-template)
  * [Modify the properties of a message template](#modify-the-properties-of-a-message-template)
  * [Insert message templates](#insert-message-templates)
  * [Create a message recipient group](#create-a-message-recipient-group)
  * [Change the properties of a message recipient group](#change-the-properties-of-a-message-recipient-group)
  * [Insert a message recipient group](#insert-a-message-recipient-group)
  * [Set up signatures](#set-up-signatures)

## Features
- Creates quote header in Outlook style for reply and forward messages that just works.
- Manages message templates for both plaintext and HTML format messages.
- Manages message recipient groups.
- Manages message signature for each mail account.
- Context menu item to initiate a new message composition to the recipients of an existing message. This is in fact similar to creating a new message by clicking the `Write` button on the Thunderbird main window. The difference is the address fields are automatically filled with the correspondent list of the message on which this context menu is executed. This menu is only available on the mail list of the main Thunderbird window.
- Context menu item to reply to a message with the original message attachments included. This menu is only available on the mail list of the main Thunderbird window.

Besides the main features above, this addon also fixes an issue related to the formatting of HTML messages created in Ms Outlook. The issue itself is not related to Advanced Composer actually but I find it annoying where sometimes when replying to such messages, the font name and color of the quoted message become different from the original one. The root cause of this issue seems to be the way Thunderbird handles inline CSS style of font names containing white space is different from Ms Outlook, e.g., Segoe UI.

See the image below for example. The `Hello world!` text lost its original format when not using Advanced Composer.

<img src="screenshots/css-font-name-fix.png" height="168" alt="CSS font name fixing" />

<br>

## Prerequisites
- The minimum version of Thunderbird supported is 91.4.
- For Linux based systems, install *msttcorefonts* package for standard fonts. Without this package you will not find any fonts listed on *General* tab and the WYSIWYG editor of the option page of this extension.
- Updating from version 1.0 to 2.* will preserve the extension data with the exception: default templates in version 1.0 (if any) will not be automatically set as default templates in version 2.*; they must be set manually again.

## Installation
From the Thunderbird add-ons manager search for *Advanced Composer* then click the button *Add to Thunderbird*.

## Quick-start Guide

### Enable or disable extension features

*Advanced Composer* features can be toggled on/off selectively. For example if the quote header is the only feature you need, you can turned off all other features.

- Open the Advanced Composer option page then go to the *General* tab.
- On the feature section, tick the features to enable or untick to disable.

![](screenshots/features.mp4){width=890 height=640}

### Change the default behavior of Thunderbird editor

- Open the Advanced Composer option page.
- On the composition section of the *General* tab:
    - Tick the checkbox `Override Thunderbird default font` and set the default font name, color, and size that will be used when composing the message.
    - Tick the checkbox `Suppress paragraph margin` to remove space between paragraphs.
    - If you want your message subject prefix to behave like Ms Outlook, e.g., `Re:` changed to `RE:`, `Fwd:` changed to `FW:`, untick the checkbox `When replying to or...`.

![](screenshots/composition-options.mp4){width=890 height=640}

### Customize the quote header

- Go to the *Quote header* tab.
- Select the account whose quote header you want to modify from the account list.
- Click on the three-dots icon then `Customize...` or double click on the preview area.
- In the quote header options dialog:
    - Click button `More` to show all options.
    - Enter the language tag in the `Datetime locale` input to change datetime format, e.g., `id-ID` for Indonesian format. Leaving it empty defaults to `en`.
    - Use the inputs in the `Field name translation` section to change or translate qoute field names and `Importance level translation` for importance levels.
    - To change the order of the quote header fields:
        - Enter the valid values 1 through to 7 in the input `Field order` separated by a space. The default value is `1 2 3 4 5 6`.
        - The header fields can also be filtered to only include specific fields. To do so, enter only the values related to the fields you want to be included.
    - Use `Line color` to change the color of the message delimiter for HTML messages. Color `rgb(181, 196, 223)` is the default color as used by the older version Ms Outlook and `rgb(225, 225, 225)` seems to be the newer one.
    - Use `Thread delimiter` to change the default message boundary of plain text messages. The format is `ReplyDelimiter|ForwardDelimiter|`. Example: `-----Pesan Asli-----|-----Pesan Diteruskan-----|`.
    - `Thread delimiter` can also be applied to HTML messages. For that purpose, append `html` to the delimiter text, e.g., `-----Messaggio originale-----|-----Messaggio inoltrato-----|html`.
    - Still about `Thread delimiter`, it can also be used to remove the message boundary line by setting the delimiter to `||html`.
    - To change the address style of the `From` header field, select the options from `Style of From field`.
    - Tick the checkbox `Include UTC offset` to include UTC offset in the `Sent` header field.
    - Tick the checkbox `Simplify name list` if you want the `To` and `Cc` fields to contain names only without mail addresses.

![](screenshots/quote-header-options.mp4){width=890 height=640}

### Create a message template

- Go to the *Template* tab.
- Click the three-dots button then `New...`.
- Click `More` button of the dialog to show all options.
- Enter the template name in the input `Name`.
- Select the message format from the `Format` selection. Select HTML if you want your template to have some styling such as italic, bold, etc.
- From the `Account` selection, choose which mail account the new template will be associated with.
- The `Copy from` can be used to copy an existing template content to the new template. `None` means no copying will be performed.

Please note these two selections ![Template filters](screenshots/filters.png) act as filters only. They do not change the format or the associated mail account of a template.

![](screenshots/create-template.mp4){width=890 height=640}

### Modify the properties of a message template

- Go to the *Template* tab.
- Select the template whose properties you want to change.
- Click the three-dots button then `More options...`.
- On the dialog box shown, the associated mail account can be changed from `Account` selection.
- There are three insertion modes to choose from `Insert mode` selection. Each mode determines where to place the template in the Thunderbird editor.
    - `Prepend` means the template will be placed at the beginning of the Thunderbird editor. The current content of the editor will be preserved.
    - `Append` means:
        - For plaintext message, the template will be placed at the end of the editor.
        - For HTML message, the template will be placed right above the signature or above the quote header if no signature is present.
        - In either case the current content of the editor will be preserved.
    - `Replace` means:
        - For plaintext message, the current content of the editor will be completely removed and replaced by the template content.
        - For HTML message, the current content above the signature will be removed and replaced by the template content.
- In addition to insertion modes, a placeholder can also be assigned to the template. It acts just like the insert modes but it is more flexible. The valid placeholder is alpha numeric plus underscore. Please note, placeholders take precedence over the insert modes.
- To assign a shortcut to the template, place the cursor in the input `Shortcut` then press `Alt` key and one of the keys `F8` to `F12`.
- Tick the check box `When I insert this template...` then enter the mail subject in the text area. When this template is inserted, the `Subject` field of the Thunderbird editor will be set to this value. Some notes to bear in mind:
    - The subject will be applied only when the `Subject` field of the Thunderbird editor is still empty, e.g., when creating a new mail message.
    - Inserting multiple templates at once by pressing `Alt+F1` will not change the mail subject even if the `Subject` field of the Thunderbird editor is still empty.
    - For reply or forward message, it is possible to append some text to the original mail subject by setting the template mail subject to something like `${SUBJECT} text_to_append`.

![](screenshots/template-properties.mp4){width=890 height=640}

### Insert message templates

- To insert one template at a time with no placeholders are present in the editor:
    - Right click anywhere in the Thunderbird editor area then point your mouse to `Message Templates`, then click the template name.
    - Alternatively press the shortcut associated with the template.
- To insert one template at a time with placeholder:
    - Write the appropriate placeholder at the location you desire in the form of `$(PLACEHOLDER)`. The placeholder must be on its own paragraph and there must be no other texts before and after it.
    - Right click anywhere in the editor area then point your mouse to `Message Templates`, then click the template name.
    - Alternatively press the shortcut associated with the template.
- To insert multiple templates at once:
    - Write the placeholders of the templates you want to insert in the editor, each on its own paragraph.
    - Press `Alt+F1`.

![](screenshots/insert-template.mp4){width=890 height=640}

### Create a message recipient group

- Go to the *Message recipients* tab.
- Click the three-dots button then `New...`.
- Click `More` button of the dialog to show all options.
- Enter the group name in the input `Name`.
- From the `Account` selection, choose which mail account the new group will be associated with.
- The `Copy from` can be used to copy an existing group content to the new one. `None` means no copying will be performed.
- For each address field:
    - Click the `Edit` button then fill in the text area with email addresses separated by comma. Each address can be in the format of *"Name \<mail@example.com\>"* or *"mail@example.com"*.
    - While the text area is active, press `Alt+R` to display the addresses by line.
    - To save the adresses click `Save` button.

![](screenshots/create-recipient-group.mp4){width=890 height=640}

### Change the properties of a message recipient group

- Go to the *Message recipients* tab.
- Select the group whose properties you want to change.
- Click the three-dots button then `More options...`.
- On the dialog box shown, the associated mail account can be changed from `Account` selection.
- In the input `Reply-to address` enter the reply-to address. Currently only one address is supported per group.

![](screenshots/group-properties.mp4){width=890 height=640}

### Insert a message recipient group

- Right click in the address fields or in the subject field of the Thunderbird editor then point your mouse to the menu item *Message Recipients*.
- Click the group name you want to insert.

![](screenshots/insert-recipient-group.mp4){width=890 height=640}

### Set up signatures

**For versions 2.4 and above**

As of version 2.4, signature handling logic has changed. Signature setup is now easier to understand and it is now possible to use a signature from file.
- To use Thunderbird signatures, disable the signature feature of Advanced composer then set up your Thunderbird signature as usual. If you are upgrading from previous versions, make sure you restore the changes you made on the Thunderbird account setting.
- To use Advanced Composer signatures, enable the signature feature.

Things to note:
- Advanced Composer signatures take precedence over Thunderbird signatures. That means when Advanced Composer signature feature is enabled, Thunderbird signatures will be ignored.
- On previous versions, in order to be able to use Thunderbird signature you need to set the content of Advanced Composer signature to `${NATIVE_SIGNATURE}`. As of version 2.4 it is not required anymore. 

**For versions 2.0 - 2.3**

Before you continue, you should turn off the default signature setting as follows:

<img src="screenshots/turn-off-account-signature.png" height="353" alt="Turn off account signature" />

<br>

Set up HTML signature
- For text only signature:
    - Go to the *Signatures* tab of the Advanced Composer option page.
    - Select the account whose signature is going to be created.
    - Write your signature in the editor.
- Complex signature
    - Open the Thunderbird account setting.
    - Click on the mail account you want to set up.
    - In the text area `Signature text:` enter your signature in HTML format then tick `Use HTML (e.g., <b>bold</b>)`.
    - Now go to *Signatures* tab of the Advanced Composer option page.
    - Select the account whose signature you want to set up.
    - In the editor enter `${NATIVE_SIGNATURE}`.

Set up plaintext signature
- The easiest way is to create a plaintext template and set it as the default template.
- Another way is similar to the complex signature in HTML format but do not tick `Use HTML (e.g., <b>bold</b>)`.

![](screenshots/setup-signature.mp4){width=890 height=640}
