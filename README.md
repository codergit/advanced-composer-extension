# Advanced Composer
A mail message composition extension for Mozilla Thunderbird.

<br>

Documentation for current release is [https://advanced-composer.gitlab.io/docs/](https://advanced-composer.gitlab.io/docs/).

For version 2.* the documentation is [here](/docs/v2/Docs-v2.md).
